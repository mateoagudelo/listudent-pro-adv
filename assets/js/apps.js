/* =================================
TOOLTIPS & POPOVERS        
=================================== */
$(function () {
	
  $('[data-toggle="tooltip"]').tooltip()
  
  $('[data-toggle="popover"]').popover()
  
})

/* =================================
DOT DOT DOT      
=================================== */
$(function() {
	$('.dots').dotdotdot();
});


/* =================================
DATEPICKER        
=================================== */
// Single Entry
$(function () {
    $('#datetimepicker1').datetimepicker();

// Inline
    $('#datetimepickerInline').datetimepicker({
        inline: true,
        sideBySide: true
    });

// Linked
	$('#datetimepickerStart').datetimepicker();
        $('#datetimepickerEnd').datetimepicker({
            useCurrent: false //Important
        });
        $("#datetimepickerStart").on("dp.change", function (e) {
            $('#datetimepickerEnd').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepickerEnd").on("dp.change", function (e) {
        $('#datetimepickerStart').data("DateTimePicker").maxDate(e.date);
    });
});

/* =================================
CARD REVEAL        
=================================== */
$(function() {
    $("body").find(".flip-me").on("click", function(a) {
        $(a.currentTarget).parents(".card").find(".card-reveal").toggleClass("active")
    })
});

/* =================================
SWIPEBOX        
=================================== */
$(window).load(function() {
    $('.swipebox').swipebox();
});

/* =================================
MIX IT UP        
=================================== */
$(function(){
	$('#container').mixItUp();
});

/* =================================
Parallax        
=================================== */
$(document).ready(function(){
	// Parallax        
    if (($(window).width() >= 1024)) {
        $(".parallax").parallax("50%", 0.3);
    } 
	// Parallax Section BG
	var pageSection = $(".parallax-section, .slider-section, .section, .title-parallax");
    pageSection.each(function(indx){
        
        if ($(this).attr("data-background")){
            $(this).css("background-image", "url(" + $(this).data("background") + ")");
        }
    });
});

/* =================================
CAROUSEL SLIDERS          
=================================== */
$(document).ready(function() {
 
  	$("#slider-one").owlCarousel({
 
      	navigation : true, // Show next and prev buttons
      	slideSpeed : 300,
      	paginationSpeed : 400,
      	singleItem:true,
      	navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  	});
  
  	$("#slider-two").owlCarousel({
 
      	navigation : true, // Show next and prev buttons
	  	slideSpeed : 300,
	  	paginationSpeed : 400,
	  	singleItem:true,
	  	navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
 
  	});
  
	// Item Slider
	$(".item-slider").owlCarousel({
	    autoPlay: 2500,
	    //stopOnHover: true,
	    items: 3,
	    itemsDesktop: [1199, 3],
	    itemsTabletSmall: [768, 3],
	    itemsMobile: [480, 1],
	    navigation: false,
	    navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	});
    
    // Full slider
    $(".full-slider").owlCarousel({
        slideSpeed : 600,
        singleItem: true,
        autoHeight: true,
        navigation: true,
        transitionStyle : "fade",
        autoPlay: true,
        rewindNav : true,
        
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    });
    
    // Gallery slider
    $(".gallery-slider").owlCarousel({
        slideSpeed : 350,
        singleItem: true,
        autoHeight: true,
        autoPlay : 5000,
        navigation: false,
        pagination: false,
        transitionStyle : "fade"
    });
 
});

