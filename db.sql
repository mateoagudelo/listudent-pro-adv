-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-10-2018 a las 03:39:28
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `listudent`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ltd_faltas`
--

CREATE TABLE `ltd_faltas` (
  `id` int(11) NOT NULL,
  `id_es` int(11) NOT NULL,
  `contenedor` int(11) NOT NULL,
  `razon` varchar(200) NOT NULL,
  `justificada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ltd_grupos`
--

CREATE TABLE `ltd_grupos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ltd_grupos`
--

INSERT INTO `ltd_grupos` (`id`, `nombre`) VALUES
(1, 'sección 10'),
(2, 'wdsd'),
(3, 'sdsdsd'),
(4, 'sdsdsd'),
(5, 'sdsdsd'),
(6, 'sdsdsd'),
(7, 'sdsdsdsd'),
(8, 'dedede'),
(9, 'dededede'),
(10, 'fdfsa'),
(11, 'dsddfs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ltd_notas`
--

CREATE TABLE `ltd_notas` (
  `id` int(11) NOT NULL,
  `id_es` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `nota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ltd_usuarios`
--

CREATE TABLE `ltd_usuarios` (
  `id` int(11) NOT NULL,
  `rango` int(11) NOT NULL DEFAULT '2',
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `contrasena` varchar(400) DEFAULT NULL,
  `grupo` int(11) NOT NULL DEFAULT '0',
  `documento` int(11) NOT NULL,
  `estado_cuenta` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ltd_usuarios`
--

INSERT INTO `ltd_usuarios` (`id`, `rango`, `nombre`, `apellidos`, `correo`, `contrasena`, `grupo`, `documento`, `estado_cuenta`) VALUES
(33, 1, 'Mateo', 'Agudelo', 'mateo@gmail.com', 'fa49d12b5c877f71ad9e7d8f5d02b6d5ae407c5a', 0, 0, 1),
(34, 2, 'Juan', 'Torres', 'juan@gmail.com', NULL, 1, 1234678854, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ltd_faltas`
--
ALTER TABLE `ltd_faltas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ltd_grupos`
--
ALTER TABLE `ltd_grupos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ltd_notas`
--
ALTER TABLE `ltd_notas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ltd_usuarios`
--
ALTER TABLE `ltd_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ltd_faltas`
--
ALTER TABLE `ltd_faltas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ltd_grupos`
--
ALTER TABLE `ltd_grupos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `ltd_notas`
--
ALTER TABLE `ltd_notas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ltd_usuarios`
--
ALTER TABLE `ltd_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
