<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?= link_tag('assets/css/project.css'); ?>
	<?= link_tag('assets/css/prettify.css'); ?>
</head>
<body>

<div class="col-md-6 col-md-offset-3">
<div class="panel panel-success">
	
	<div class="panel-heading">Registro</div>
	<div class="panel-body">
	
<?php
$nombre = array('type' => 'text','name' => 'nombre','id' => 'nombre', 'class' => 'form-control', 'value' => set_value('nombre'));
$apellidos = array('type' => 'text','name' => 'apellidos','id' => 'apellidos', 'class' => 'form-control', 'value' => set_value('apellidos')); 
$correo = array('type' => 'mail','name' => 'correo','id' => 'correo', 'class' => 'form-control', 'value' => set_value('correo'), 'autocomplete' => 'off');
$contrasena = array('type' => 'password','name' => 'contrasena','id' => 'contrasena', 'class' => 'form-control', 'value' => set_value('contrasena'));
$contrasena_c = array('type' => 'password','name' => 'contrasena_confirmar','id' => 'contrasena_confirmar', 'class' => 'form-control', 'value' => set_value('contrasena_confirmar'));
$btn = array('type' => 'submit','value' => 'Registrarme!', 'class' => 'btn btn-success');   
?>

	<?= form_open(); ?>
	<?= validation_errors(); ?>

	<?= form_label('Nombre: '); ?>
	<?= form_input($nombre); ?>
	<br>

	<?= form_label('Apellidos: '); ?>
	<?= form_input($apellidos); ?>
	<br>

	<?= form_label('Correo: '); ?>
	<?= form_input($correo); ?>
	<br>

	<?= form_label('Contraseña: '); ?>
	<?= form_input($contrasena); ?>
	<br>

	<?= form_label('Confirmar contraseña: '); ?>
	<?= form_input($contrasena_c); ?>
	<br>

	<?= form_input($btn); ?>
	<?= form_close(); ?>

	</div>

</div>
</div>

</body>
</html>