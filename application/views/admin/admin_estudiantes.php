<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item">Estudiantes</li>
</ol>
</nav>
</div>
<div class="col-md-6 col-sm-12 text-right">
<a href="<?= base_url('admin/estudiantes/nuevo/'); ?>"><div class="btn btn-primary">Añadir nuevo</div></a>
</div>
</div>
</div>


<!-- incio -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix mb-20">
<div class="pull-left">
<h4 class="text-blue">Estudiantes</h4>
<p class="font-14">Lista de estudiantes</p>
</div>
</div>
<div class="row">

<?php if ($estudiantes): ?>
		<table class="table table-hover">
      <thead>
			<tr>
        	<th>#</th>
       	 	<th>Nombre</th>
       	 	<th>Apellidos</th>
       	 	<th>Correo</th>
          <th>Documento</th>
       	 	<th>Grupo/Sección</th>
       	 	<th>Estado cuenta</th>
          <th>Opciones</th>
      </tr>
    </thead>

		<?php foreach ($estudiantes as $estudiante): ?>
      <tbody>
	        <tr>
        		<td><?= $estudiante->id; ?></td>
        		<td><?= $estudiante->nombre; ?></td>
        		<td><?= $estudiante->apellidos; ?></td>
        		<td><?= $estudiante->correo; ?></td>
            <td><?= $estudiante->documento; ?></td>
        		<td><a href="<?= base_url('admin/grupos/ver/'.$estudiante->grupo.'/'); ?>"><?= $modelo->Obtener_Grupo($estudiante->grupo); ?></a></td>
        		<td><?php if ($estudiante->estado_cuenta == 1) { echo 'Activa'; } else { echo 'No activa'; } ?></td>
            <td>
              <div class="dropdown">
                <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                <i class="fa fa-ellipsis-h"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="<?= base_url('admin/estudiantes/editar/'.$estudiante->id.'/'); ?>"><i class="fa fa-pencil"></i> Editar</a>
                <a class="dropdown-item" href="<?= base_url('admin/estudiantes/eliminar/'.$estudiante->id.'/'); ?>"><i class="fa fa-ban"></i> Eliminar</a>
              </div>
        </div>
            </td>
        	</tr>
      </tbody>
		<?php endforeach ?>

		</table>
		<?= $this->pagination->create_links(); ?>
<?php else: ?>
	<div class="alert alert-warning"><strong>Oops! </strong> Al parecer no hay estudiantes!</div>
<?php endif ?>
</div>
</div>



</div>


<?= link_js_admin(); ?>

</body>
</html>