<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item active" aria-current="page">Notas</li>
<li class="breadcrumb-item active" aria-current="page">Editar</li>
<?php foreach ($notas as $nota): ?>
<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('/admin/notas/ver_todas/'.$nota->id_es.'/'); ?>"><?= $modelo->Get_Nombre_Estudiante($nota->id_es); ?></a></li>
</ol>
</nav>
</div>

</div>
</div>

<!-- Default Basic Forms Start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix">
<div class="pull-left">
<h4 class="text-blue">Editar nota</h4>
<p class="mb-30 font-14">Datos requeridos</p>
</div>
</div>
	
	<div class="panel panel-primary">
		<div class="panel-body">
			
			<?= form_open(); ?>

				<label for="nombre">Nombre</label>
				<input type="text" value="<?= $nota->nombre; ?>" class="form-control" name="nombre">
				<br>

				<label for="">Nota</label>
				<input type="number" value="<?= $nota->nota; ?>" class="form-control" name="nota">
				<br>

				<input type="submit" class="btn btn-success" value="Actualizar" name="">
				<?php endforeach ?>
			<?= form_close(); ?>

		</div>
	</div>
	

</div>

<?= link_js_admin(); ?>

</body>
</html>