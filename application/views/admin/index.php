<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="row clearfix progress-box">

<div class="col-lg-3 col-md-6 col-sm-12 mb-30">
<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
<div class="project-info clearfix">
<div class="project-info-left">
<div class="icon box-shadow bg-blue text-white">
<i class="fa fa-briefcase"></i>
</div>
</div>
<div class="project-info-right">
<span class="no text-blue weight-500 font-24"><?= $modelo->get_Numero_Grupos(); ?></span>
<p class="weight-400 font-18">Grupos</p>
</div>
</div>
</div>
</div>

<div class="col-lg-3 col-md-6 col-sm-12 mb-30">
<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
<div class="project-info clearfix">
<div class="project-info-left">
<div class="icon box-shadow bg-blue text-white">
<i class="fa fa-briefcase"></i>
</div>
</div>
<div class="project-info-right">
<span class="no text-blue weight-500 font-24"><?= $modelo->get_Numero_Estudiantes(); ?></span>
<p class="weight-400 font-18">Estudiantes</p>
</div>
</div>
</div>
</div>

</div>
</div>
</div>

<?= link_js_admin(); ?>
</body>
</html>