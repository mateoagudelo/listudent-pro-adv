<!DOCTYPE html>
<html>
<head>
<title></title>
<?= link_tag('assets/css/project.css'); ?>
<?= link_tag('assets/css/prettify.css'); ?>
</head>
<body>
<?= menu(); ?>


<div class="container">

<div class="row"><h3>Grupos > Eliminar</h3></div><br>

<div class="panel panel-default">
	<div class="panel-body">
		<center>
			<h2>¿Esta segur@ que desea eliminar esta falta?</h2>
			<?= form_open(); ?>
				<input type="hidden" value="" name="id">
				<input type="submit" value="Si" class="btn btn-danger">
			<?= form_close(); ?>
			<br>
				<a href="<?= base_url('admin/faltas/'); ?>"><input type="submit" value="Cancelar" class="btn btn-primary" name=""></a>
		</center>
	</div>
</div>

</div>


<?= link_tag_script('assets/js/jquery-2.1.4.min.js'); ?>
<?= link_tag_script('assets/js/bootstrap.min.js'); ?>
</body>
</html>