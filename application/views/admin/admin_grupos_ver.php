<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item"><a href="<?= base_url('admin/grupos/'); ?>">Grupos</a></li>

<?php 


if ($estudiantes) {
	foreach ($estudiantes as $grupo) {
		$grupo_final = $grupo->grupo;
	}
}

$mostrar_grupo = $modelo->Obtener_Grupo($grupo_final);

 ?>

<li class="breadcrumb-item active" aria-current="page"><?= $mostrar_grupo ?></li>
</ol>
</nav>
</div>
</div>
</div>

<!-- incio -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix mb-20">
<div class="pull-left">
<h4 class="text-blue"><?= $mostrar_grupo ?></h4>
<p class="font-14">Lista de estudiantes del grupo</p>
</div>
</div>
<div class="row">
		

<?php if ($estudiantes): ?>
		<table class="table table-hover">
			<thead>
			<tr>
        	<th>#</th>
       	 	<th>Estudiante</th>
       	 	<th>Correo</th>
       	 	<th>Documento</th>
       	 	<th>Cuenta</th>
       	 	<th>Notas</th>
			<th>Faltas</th>
        	</tr>
			</thead>

		<?php foreach ($estudiantes as $estudiante): ?>
	        <tr>
        		<td><?= $estudiante->id; ?></td>
        		<td><?= $estudiante->nombre . ' ' . $estudiante->apellidos; ?></td>
        		<td><?= $estudiante->correo; ?></td>
        		<td><?= $estudiante->documento; ?></td>
        		<td><?php if ($estudiante->estado_cuenta == 1) { echo 'Activa'; } else { echo 'No activa'; } ?></td>

				<td>
				<div class="dropdown">
					<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
					<i class="fa fa-ellipsis-h"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="<?= base_url('admin/notas/ver_todas/'.$estudiante->id.'/'); ?>"><i class="fa fa-eye"></i> Todas</a>
					<a class="dropdown-item" href="<?= base_url('admin/notas/nueva/'.$estudiante->id.'/'); ?>"><i class="fa fa-plus"></i> Añadir</a>
					</div>
				</div>
				</td>

				<td>
				<div class="dropdown">
					<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
					<i class="fa fa-ellipsis-h"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="<?= base_url('admin/faltas/ver_todas/'.$estudiante->id.'/'); ?>"><i class="fa fa-eye"></i> Todas</a>
					<a class="dropdown-item" href="<?= base_url('admin/faltas/nueva/'.$estudiante->id.'/'); ?>"><i class="fa fa-plus"></i> Añadir</a>
					</div>
				</div>				
				</td>
        	</tr>
		<?php endforeach ?>

		</table>
<?php else: ?>
	</div>
	<div class="alert alert-warning"><strong>Oops! </strong> Al parecer no hay estudiantes!</div>
<?php endif ?>

	</div>
</div>

</div>

</div>
</div>
</div>
</div>
<!-- fin -->

<?= link_js_admin(); ?>
</body>
</html>