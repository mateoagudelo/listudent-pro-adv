<!DOCTYPE html>
<html>
<head>
<title></title>
<?= link_tag('assets/css/project.css'); ?>
<?= link_tag('assets/css/prettify.css'); ?>
</head>
<body>
<?= menu(); ?>

<div class="container">

<div class="row"><h3>Definitiva > Asignar</h3></div><br>

	<div class="panel panel-primary">
		<div class="panel-heading"><strong>Definitiva</strong></div>
		<div class="panel-body">
			
			<?php foreach ($definitivas as $definitiva): ?>
			<?= form_open();  ?>
			<label>Estudiante: </label>
			<input type="text" class="form-control" value="<?= $estudiante; ?>" readonly="">
			<br>

			<label>Nota: </label>
			<input type="number" name="nota" id="nota" value="<?= $definitiva->nota; ?>" class="form-control" required="">
			<br>

			<input type="submit" class="btn btn-success" value="Actualizar">
			<?= form_close();  ?>
			<?php endforeach ?>

		</div>
	</div>
</div>

<?= link_tag_script('assets/js/jquery-2.1.4.min.js'); ?>
<?= link_tag_script('assets/js/bootstrap.min.js'); ?>
</body>
</html>