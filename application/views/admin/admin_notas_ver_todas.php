<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item active" aria-current="page">Notas</li>
<li class="breadcrumb-item active" aria-current="page"><?= $estudiante; ?></li>
</ol>
</nav>
</div>

<div class="col-md-6 col-sm-12 text-right">
<a href="<?= base_url('admin/notas/nueva/'.$anadir_otra_id.'/'); ?>"><div class="btn btn-primary">Añadir nueva</div></a>
</div>

</div>
</div>

<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix mb-20">
<div class="pull-left">
<h4 class="text-blue">Notas de <?= $estudiante; ?></h4>
<p class="font-14">Lista de notas</p>
</div>
</div>
<div class="row">

		<?php if ($notas): ?>
		<table class="table table-hover">
			<thead>
			<tr>
        	<th>#</th>
       	 	<th>Tarea/Taller/Examén</th>
       	 	<th>Nota</th>
       	 	<th>Acciones</th>
        	</tr>
        </thead>

		<?php foreach ($notas as $nota): ?>
	        <tr>
        		<td><?= $nota->id; ?></td>
        		<td><?= $nota->nombre; ?></td>
        		<td><?= $nota->nota; ?></td>
        		<td><a href="<?= base_url('admin/notas/editar/'.$nota->id.'/'); ?>"><button class="btn btn-warning btn-xs">editar</button></a> <a href="<?= base_url('admin/notas/eliminar/'.$nota->id.'/'); ?>"><button class="btn btn-danger btn-xs">eliminar</button></a></td>
        	</tr>
		<?php endforeach ?>

		</table>
		
		<?php else: ?>
			<div class="alert alert-warning"><strong>Oops! </strong> Al parecer no hay entradas!</div>
		<?php endif ?>


	</div>
</div>
</div>

<?= link_js_admin(); ?>

</body>
</html>