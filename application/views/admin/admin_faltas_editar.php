<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item"><a href="<?= base_url('admin/faltas/'); ?>">Faltas</a></li>
<li class="breadcrumb-item active" aria-current="page">Nuevo</li>
</ol>
</nav>
</div>
</div>
</div>

<!-- Default Basic Forms Start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix">
<div class="pull-left">
<h4 class="text-blue">Editar falta</h4>
<p class="mb-30 font-14">Datos requeridos</p>
</div>
</div>
	<div class="panel panel-primary">
		<div class="panel-body">
			
			<?= form_open(); ?>

			<?php foreach ($faltas as $falta): ?>

			<label>Razón: </label>
			<input type="text" class="form-control" value="<?= $falta->razon; ?>" name="razon">
			<br>

			<label>Justificación: </label>
			<select class="form-control" name="justificada">
				<?php if ($falta->justificada == 0): ?>
					<option value="1">Si</option>
					<option value="0" selected="">No</option>
				<?php elseif($falta->justificada == 1): ?>
					<option value="1" selected="">Si</option>
					<option value="0">No</option>					
				<?php endif ?>
			</select>
			<br>

			<input type="submit" class="btn btn-success" value="Actualizar">
			<?php endforeach ?>

			<?= form_close(); ?>

		</div>
	</div>
</div>

<?= link_js_admin(); ?>
</body>
</html>