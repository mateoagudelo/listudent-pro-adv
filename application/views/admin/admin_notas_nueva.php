<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item active" aria-current="page">Notas</li>
<li class="breadcrumb-item active" aria-current="page">Nueva</li>
<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('/admin/notas/ver_todas/'.$id_estudiante.'/'); ?>"><?= $estudiante; ?></a></li>
</ol>
</nav>
</div>

</div>
</div>

<!-- Default Basic Forms Start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix">
<div class="pull-left">
<h4 class="text-blue">Nueva nota</h4>
<p class="mb-30 font-14">Datos requeridos</p>
</div>
</div>

<div class="panel panel-primary">
	<div class="panel-body">
		
		<?= form_open(); ?>

		<label>Taller/Tarea</label>
		<input type="text" class="form-control" name="nombre" required="">
		<br>

		<label>Nota</label>
		<input type="number" class="form-control" name="nota" required="">
		<br>		

		<input type="submit" class="btn btn-success" value="Añadir">
		<?= form_close(); ?>

	</div>
</div>

</div>

<?= link_js_admin(); ?>

</body>
</html>