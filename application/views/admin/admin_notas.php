<!DOCTYPE html>
<html>
<head>
<title></title>
<?= link_tag('assets/css/project.css'); ?>
<?= link_tag('assets/css/prettify.css'); ?>
</head>
<body>
<?= menu(); ?>


<div class="container">

<div class="row"><h3>Notas > Todas</h3></div><br>	
	
<div class="panel panel-primary">
	<div class="panel-heading"><strong>Notas</strong> <div class="pull-right"><a href="<?= base_url('admin/grupos/'); ?>"><button class="btn btn-success btn-xs">nueva</button></a></div></div>
	<div class="panel-body">
		

	<?php if ($notas): ?>
		<table class="table table-hover">
			<tr>
        	<th>#</th>
       	 	<th>Estudiante</th>
       	 	<th>Tarea/Taller/Examén</th>
       	 	<th>Nota</th>
       	 	<th>Acciones</th>
        	</tr>

		<?php foreach ($notas as $nota): ?>
	        <tr>
        		<td><?= $nota->id; ?></td>
        		<td><?= $modelo->Get_Nombre_Estudiante($nota->id_es); ?></td>
        		<td><?= $nota->nombre; ?></td>
        		<td><?= $nota->nota; ?></td>
        		<td><a href="<?= base_url('admin/notas/editar/'.$nota->id.'/'); ?>"><button class="btn btn-warning btn-xs">Editar</button></a> <a href="<?= base_url('admin/notas/eliminar/'.$nota->id.'/') ?>"><button class="btn btn-danger btn-xs">eliminar</button></a> <a href="<?= base_url('admin/notas/nueva/'.$nota->id_es.'/'); ?>"><button class="btn btn-success btn-xs">añadir otra</button></a></td>
        	</tr>
		<?php endforeach ?>

		</table>
		<?= $this->pagination->create_links(); ?>
	<?php else: ?>
		<div class="alert alert-warning"><strong>Oops! </strong> Al parecer no hay notas!</div>
	<?php endif ?>


	</div>
</div>

</div>


<?= link_tag_script('assets/js/jquery-2.1.4.min.js'); ?>
<?= link_tag_script('assets/js/bootstrap.min.js'); ?>
</body>
</html>