<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item"><a href="<?= base_url('admin/estudiantes/'); ?>">Estudiantes</a></li>
<li class="breadcrumb-item active" aria-current="page">Nuevo</li>
</ol>
</nav>
</div>
</div>
</div>

<!-- Default Basic Forms Start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix">
<div class="pull-left">
<h4 class="text-blue">Nuevo estudiante</h4>
<p class="mb-30 font-14">Datos requeridos</p>
</div>
</div>

<div class="panel panel-primary">
<div class="panel-body">
<?php if (validation_errors()): ?>
<div class="alert alert-danger"><?= validation_errors(); ?></div>
<?php endif ?>
<?= form_open(); ?>

<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label" for="nombre">Nombre(s)</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="nombre" id="nombre" type="text" placeholder="Ej. Juan">
</div>
</div>

<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label" for="apellidos">Apellido(s)</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="apellidos" id="apellidos" type="text" placeholder="Ej. Torres">
</div>
</div>

<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label" for="correo">Correo</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="correo" id="correo" type="email" placeholder="Ej. juan_torres@mail.com">
</div>
</div>

<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label" for="contrasena">Documento de identidad</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="documento" id="documento" type="number" placeholder="Ej. 18635472425">
</div>
</div>

<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label" for="grupo">Grupo/Sección</label>
<div class="col-sm-12 col-md-10">

<select class="form-control" name="grupo" id="grupo">
<option selected="">-- seleccione --</option>
<?php foreach ($grupos as $grupo): ?>
	<option value="<?= $grupo->id; ?>"><?= $grupo->nombre; ?></option>
<?php endforeach ?>
</select>

</div>
</div>


<div class="row">
<div class="col-sm-6">
<div class="input-group">
<input type="submit" value="Añadir" class="btn btn-outline-primary">
</div>

</div>

<?= form_close(); ?>
</div>

</div>
</div>
</div>

<?= link_js_admin(); ?>
</body>
</html>