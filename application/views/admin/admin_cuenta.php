<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item active" aria-current="page">Mi Cuenta</li>
</ol>
</nav>
</div>
</div>
</div>

<!-- Default Basic Forms Start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix">
<div class="pull-left">
<h4 class="text-blue">Mi Cuenta</h4>
<p class="mb-30 font-14">Configuración básica</p>
</div>
</div>

<?= form_open(); ?>
<?php foreach ($cuentas as $cuenta): ?>

<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label">Nombres(s)</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="nombre" id="nombre" type="text" value="<?= $cuenta->nombre; ?>" placeholder="Ej. juan">
</div>
</div>
<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label">Apellido(s)</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="apellidos" id="apellidos" type="search" value="<?= $cuenta->apellidos; ?>" placeholder="Ej. Alvarez">
</div>
</div>
<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label">Correo electrónico</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" value="<?= $cuenta->correo; ?>" placeholder="Ej. juanalvarez@example.com" type="email">
</div>
</div>

<div class="row">
<div class="col-sm-6">
<div class="input-group">
<input type="submit" value="Actualizar datos" class="btn btn-outline-primary">
&nbsp;
<a href="<?= base_url('admin/cuenta/seguridad/'); ?>" class="btn btn-outline-danger">Cambiar contraseña</a>
</div>

</div>

<?php endforeach ?>
<?= form_close(); ?>
</div>

<?= link_js_admin(); ?>
</body>
</html>