<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item"><a href="<?= base_url('admin/grupos/'); ?>">Grupos</a></li>
<li class="breadcrumb-item active" aria-current="page">Editar grupo</li>
</ol>
</nav>
</div>
</div>
</div>

<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix mb-20">
<div class="pull-left">
<h4 class="text-blue">Editar grupo</h4>
<p class="font-14">Edición de un grupo/sección</p>
</div>
</div>

<?= form_open(); ?>
<?php foreach ($grupos as $grupo): ?>
<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label">Nombre</label>
<div class="col-sm-12 col-md-10">
<?= grupos_editar_nombre($grupo->nombre); ?>
</div>
</div>

<div class="row">
<div class="col-sm-6">
<div class="input-group">
<input type="submit" value="Guardar" class="btn btn-outline-primary">
</div>
</div>
<?php endforeach ?>
<?= form_close(); ?>

<?= link_js_admin(); ?>
</body>
</html>