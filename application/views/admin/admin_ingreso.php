<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>

<div class="login-wrap customscroll d-flex align-items-center flex-wrap justify-content-center pd-20">
		<div class="login-box bg-white box-shadow pd-30 border-radius-5">
			<h2 class="text-center mb-30">Panel</h2>
			<?= form_open(); ?>
				<?= validation_errors(); ?>
				<div class="input-group custom input-group-lg">
					<input type="text" name="correo" id="correo" class="form-control" placeholder="Correo eléctronico" value="<?php echo set_value('correo') ?>" required="">
					<div class="input-group-append custom">
						<span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
					</div>
				</div>
				<div class="input-group custom input-group-lg">
					<input type="password" name="contrasena" id="contrasena" class="form-control" placeholder="**********" value="<?php echo set_value('contrasena') ?>" required="">
					<div class="input-group-append custom">
						<span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
					<div class="input-group">
					<input type="submit" value="Ingresar" class="btn btn-outline-primary btn-lg btn-block">
					</div>
				</div>
				</div>
			<?= form_close(); ?>
		</div>
	</div>

</body>
</html>