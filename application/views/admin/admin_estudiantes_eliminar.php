<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.php">Estudiantes</a></li>
<li class="breadcrumb-item active" aria-current="page">Eliminar</li>
</ol>
</nav>
</div>
</div>
</div>

<!-- Default Basic Forms Start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix">
</div>

<div class="panel panel-default">
	<div class="panel-body">
		<center>
			<h2>¿Esta segur@ que desea eliminar este estudiante?</h2>
			<?= form_open(); ?>
				<input type="hidden" value="" name="id">
				<input type="submit" value="Si" class="btn btn-danger">
			<?= form_close(); ?>
			<br>
				<a href="<?= base_url('admin/estudiantes/'); ?>"><input type="submit" value="Cancelar" class="btn btn-primary" name=""></a>
		</center>
	</div>
</div>

</div>


<?= link_js_admin(); ?>
</body>
</html>