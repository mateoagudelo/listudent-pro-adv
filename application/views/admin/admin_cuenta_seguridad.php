<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item"><a href="<?php echo base_url('/admin/cuenta/'); ?>">Mi Cuenta</a></li>
<li class="breadcrumb-item active" aria-current="page">Seguridad</li>
</ol>
</nav>
</div>
</div>
</div>


<!-- Default Basic Forms Start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">

<?php if (validation_errors()): ?>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
<strong>Error!</strong> <?= validation_errors(); ?>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">×</span>
</button>
</div>
<?php endif ?>


<div class="clearfix">
<div class="pull-left">
<h4 class="text-blue">Seguridad</h4>
<p class="mb-30 font-14">Cambio de contraseña</p>
</div>
</div>

<?= form_open(); ?>

<div class="form-group row">

<label class="col-sm-12 col-md-2 col-form-label" for="contrasena_vieja">Contraseña actual</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="contrasena_vieja" id="contrasena_vieja" type="password" placeholder="*************">
</div>
</div>
<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label" for="contrasena_nueva">Contraseña nueva</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="contrasena_nueva" id="contrasena_nueva" type="password" placeholder="*************">
</div>
</div>
<div class="form-group row">
<label class="col-sm-12 col-md-2 col-form-label" for="contrasena_nueva_r">Repetir contraseña nueva</label>
<div class="col-sm-12 col-md-10">
<input class="form-control" name="contrasena_nueva_r" id="contrasena_nueva_r" placeholder="*************" type="password">
</div>
</div>

<div class="row">
<div class="col-sm-6">
<div class="input-group">
<input type="submit" value="Actualizar contraseña" class="btn btn-outline-danger">
</div>
</div>

<?= form_close(); ?>
</div>
<!-- Default Basic Forms End -->

<?= link_js_admin(); ?>
</body>
</html>