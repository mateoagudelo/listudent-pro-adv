<!DOCTYPE html>
<html>
<head>
<?= inicial(); ?>
</head>
<body>
<?= menu(); ?>
<?= barra_sup(); ?>

<div class="main-container">
<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
<div class="min-height-200px">
<div class="page-header">
<div class="row">
<div class="col-md-6 col-sm-12">
<nav aria-label="breadcrumb" role="navigation">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('admin/'); ?>">Inicio</a></li>
<li class="breadcrumb-item active" aria-current="page">Grupos</li>
</ol>
</nav>
</div>

</div>
</div>

<!-- incio -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30" id="nuevo">
<div class="clearfix mb-20">
<div class="pull-left">
<h4 class="text-blue">Nuevo</h4>
</div>
</div>
<div class="row">

<?= form_open(); ?>

<input class="form-control" name="nombre" id="nombre" type="text" placeholder="Ej. grupo #1" required="" style="margin-left: 15px; margin-top: -15px;">
<br>
<input type="submit" value="Añadir" class="btn btn-outline-primary" style="margin-left: 15px; margin-top: -15px;">

<?= form_close(); ?>

</div>
</div>
</div>

<!-- incio -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
<div class="clearfix mb-20">
<div class="pull-left">
<h4 class="text-blue">Grupos</h4>
<p class="font-14">Lista de grupos/secciones</p>
</div>
</div>
<div class="row">
					
<?php if ($grupos): ?>
		<table class="table">
		<thead>
			<tr>
        	<th scope="col">#</th>
       	 	<th scope="col">Nombre</th>
       	 	<th scope="col">Opciones</th>
        	</tr>
		</thead>

	<?php foreach ($grupos as $grupo): ?>
		<tbody>
	        <tr>
        		<td><a href="<?= base_url('admin/grupos/ver/'.$grupo->id.'/');  ?>"><?= $grupo->id; ?></a></td>
        		<td><a href="<?= base_url('admin/grupos/ver/'.$grupo->id.'/');  ?>"><?= $grupo->nombre; ?></a></td>
				<th><div class="dropdown">
					<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
					<i class="fa fa-ellipsis-h"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="#"><i class="fa fa-eye"></i> Ver</a>
					<a class="dropdown-item" href="<?= base_url('admin/grupos/editar/'.$grupo->id.'/'); ?>"><i class="fa fa-pencil"></i> Editar</a>
					</div>
				</div></th>
        	</tr>
		</tbody>
	<?php endforeach ?>

		</table>
		<?= $this->pagination->create_links(); ?>
<?php else: ?>
	<div class="alert alert-warning"><strong>Oops! </strong> Al parecer no hay grupos!</div>
<?php endif ?>

</div>
</div>
</div>
</div>
<!-- fin -->

<?= link_js_admin(); ?>

</body>
</html>