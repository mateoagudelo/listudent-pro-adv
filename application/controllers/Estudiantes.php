<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estudiantes extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form','url','html','plantilla'));
		$this->load->library('form_validation');
	}

	public function index() {
	}

	public function registro() {
		if ($this->session->userdata('logeado') == false) {

		if ($this->input->post()) {
			//aplicando reglas de validación
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[5]|max_length[50]');
			$this->form_validation->set_rules('apellidos', 'Apellidos', 'required|min_length[5]|max_length[50]');
			$this->form_validation->set_rules('correo', 'Correo', 'valid_email|trim|required|min_length[5]|max_length[150]');
			$this->form_validation->set_rules('contrasena', 'Contraseña', 'required|min_length[5]');
			$this->form_validation->set_rules('contrasena_confirmar', 'Confirmar Contraseña', 'required|matches[contrasena]');


			if ($this->form_validation->run()) {
				$nombre = $this->input->post('nombre');
				$apellidos = $this->input->post('apellidos');
				$correo = $this->input->post('correo');
				$contrasena = $this->input->post('contrasena');
				$this->load->model('Modelo_estudiantes');
				$this->Modelo_estudiantes->Nuevo($nombre, $apellidos, $correo, $contrasena);
				redirect('estudiantes/ingreso');
			} else {
				$this->load->view('estudiantes/registro');
			}


		} else {
			$this->load->view('estudiantes/registro');
		}

		} else {
			redirect('estudiantes');
		}

	}

}