<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form','url','html','admin'));
		$this->load->library('form_validation');
	}

	public function index() {
		if ($this->logeado_admin()) {
			$this->load->model('Modelo_admin');
			$data['modelo'] = $this->Modelo_admin;
			$this->load->view('admin/index', $data);
		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	private function logeado_admin() {
		if ($this->session->userdata('logeado_admin')) {
			return true;
		} else {
			return false;
		}
	}

	public function grupos() {
		if ($this->logeado_admin()) {

			$this->load->model('Modelo_admin');

			if ($this->input->post()) {
				$this->Modelo_admin->Nuevo_Grupo($this->input->post('nombre'));
				redirect(base_url('admin/grupos/'));
			}


			$config['base_url'] = base_url().'admin/grupos'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
			$config['total_rows'] = $this->Modelo_admin->Filas_Grupos();//calcula el número de filas
			$this->load->library('pagination');
			$this->pagination->initialize($config); //inicializamos la paginación		
			$data['grupos'] = $this->Modelo_admin->Total_Paginados_Grupos($this->pagination->per_page, $this->uri->segment(3));
			$this->load->view('admin/admin_grupos', $data);
		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function grupos_editar($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		if ($this->input->post()) {
			$this->Modelo_admin->Actualizar_Grupo($id, $this->input->post('nombre'), $this->input->post('descripcion'));
			redirect(base_url('admin/grupos/editar/'.$id.'/'));
		} else {
			$data['grupos'] = $this->Modelo_admin->Editar_Grupo($id);
			$this->load->view('admin/admin_grupos_editar', $data);
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function grupos_ver($id) {
		if($this->logeado_admin()) {

			$this->load->model('Modelo_admin');
			$data['estudiantes'] = $this->Modelo_admin->Get_Estudiantes_Por_Grupo($id);
			$data['grupo'] = $this->Modelo_admin->Obtener_Grupo($id);
			$data['modelo'] = $this->Modelo_admin;
			$this->load->view('admin/admin_grupos_ver', $data);

		} else {
			redirect(base_url('admin/ingreso/'));
		}

	}

	public function grupos_eliminar($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		if ($this->input->post()) {
			$this->Modelo_admin->Eliminar_Grupo($id);
			redirect(base_url('admin/grupos/'));
		} else {
			$data['grupos'] = $this->Modelo_admin->Editar_Grupo($id);
			$this->load->view('admin/admin_grupos_eliminar', $data);
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function grupos_nuevo() {
		if ($this->logeado_admin()) {

		if ($this->input->post()) {
			$this->load->model('Modelo_admin');
			$this->Modelo_admin->Nuevo_Grupo($this->input->post('nombre'));
			redirect(base_url('admin/grupos/'));
		} else {
			$this->load->view('admin/admin_grupos_nuevo');
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}


	public function estudiantes() {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		$config['base_url'] = base_url().'admin/estudiantes'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
		$config['total_rows'] = $this->Modelo_admin->Filas_Estudiantes();//calcula el número de filas
		$this->load->library('pagination');
		$this->pagination->initialize($config); //inicializamos la paginación		
		$data['estudiantes'] = $this->Modelo_admin->Total_Paginados_Estudiantes($this->pagination->per_page, $this->uri->segment(3));
		$data['modelo'] = $this->Modelo_admin;
		$this->load->view('admin/admin_estudiantes', $data);

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function estudiantes_editar($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		if ($this->input->post()) {
			$this->Modelo_admin->Actualizar_Estudiante($id, $this->input->post('nombre'), $this->input->post('apellidos'), $this->input->post('correo'), $this->input->post('grupo'), $this->input->post('documento'), $this->input->post('estado_cuenta'));
			redirect(base_url('admin/estudiantes/editar/'.$id.'/'));
		} else {
			$data['modelo'] = $this->Modelo_admin;
			$data['grupos'] = $this->Modelo_admin->Obtener_Grupos($id);
			$data['estudiantes'] = $this->Modelo_admin->Editar_Estudiantes($id);
			$this->load->view('admin/admin_estudiantes_editar', $data);
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function estudiantes_nuevo() {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		$data['grupos'] = $this->Modelo_admin->Obtener_Grupos_Nuevo();
		if ($this->input->post()) {
			
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[4]|max_length[100]');
			$this->form_validation->set_rules('apellidos', 'Apellidos', 'required|min_length[5]|max_length[100]');
			$this->form_validation->set_rules('correo', 'Correo', 'trim|valid_email|required|min_length[5]|max_length[100]');
			$this->form_validation->set_rules('documento', 'documentos', 'required|min_length[5]|max_length[100]');

			if ($this->form_validation->run()) {
				$this->Modelo_admin->Nuevo_Estudiante($this->input->post('nombre'), $this->input->post('apellidos'), $this->input->post('correo'), $this->input->post('grupo'), $this->input->post('documento'));
				redirect(base_url('admin/estudiantes/'));
			} else {
				$this->load->view('admin/admin_estudiantes_nuevo', $data);
			}


		} else {
			$this->load->view('admin/admin_estudiantes_nuevo', $data);
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}		
	}

	public function estudiantes_eliminar($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		if ($this->input->post()) {
			$this->Modelo_admin->Eliminar_Estudiante($id);
			redirect(base_url('admin/estudiantes/'));
		} else {
			$this->load->view('admin/admin_estudiantes_eliminar');
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	/** NOTAS **/

	public function notas() {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin_notas');
		$config['base_url'] = base_url().'admin/notas'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
		$config['total_rows'] = $this->Modelo_admin_notas->Filas_Notas();//calcula el número de filas
		$this->load->library('pagination');
		$this->pagination->initialize($config); //inicializamos la paginación		
		$data['notas'] = $this->Modelo_admin_notas->Total_Paginados_Notas($this->pagination->per_page, $this->uri->segment(3));
		$data['modelo'] = $this->Modelo_admin_notas;
		$this->load->view('admin/admin_notas', $data);

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function notas_eliminar($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin_notas');
		if ($this->input->post()) {
			$this->Modelo_admin_notas->Eliminar_Nota($id);
			redirect(base_url('admin/notas/'));
		} else {
			$this->load->view('admin/admin_notas_eliminar');
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function notas_editar($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin_notas');
		$data['notas'] = $this->Modelo_admin_notas->Get_Nota($id);
		$data['modelo'] =$this->Modelo_admin_notas;
		if ($this->input->post()) {
			$this->Modelo_admin_notas->Actualizar_Nota($id, $this->input->post('nombre'), $this->input->post('nota'));
			redirect(base_url('admin/notas/editar/'.$id.'/'));
		} else {
			$this->load->view('admin/admin_notas_editar', $data);
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function notas_nueva($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin_notas');
		$data['estudiante'] = $this->Modelo_admin_notas->Get_Nombre_Estudiante($id);
		$data['id_estudiante'] = $id;
		if ($this->input->post()) {
			$this->Modelo_admin_notas->Nueva_Nota($id, $this->input->post('nombre'), $this->input->post('nota'));
			redirect(base_url('admin/notas/'));
		} else {
			$this->load->view('admin/admin_notas_nueva', $data);
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}


	/*** FALTAS ***/

	public function faltas() {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		$this->load->model('Modelo_admin_notas');
		$config['base_url'] = base_url().'admin/faltas'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
		$config['total_rows'] = $this->Modelo_admin->Filas_Faltas();//calcula el número de filas
		$this->load->library('pagination');
		$this->pagination->initialize($config); //inicializamos la paginación		
		$data['faltas'] = $this->Modelo_admin->Total_Paginados_Faltas($this->pagination->per_page, $this->uri->segment(3));
		$data['modelo'] = $this->Modelo_admin_notas;
		$this->load->view('admin/admin_faltas', $data);

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function faltas_eliminar($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		if ($this->input->post()) {
			$this->Modelo_admin->Eliminar_Falta($id);
			redirect(base_url('admin/faltas/'));
		} else {
			$this->load->view('admin/admin_faltas_eliminar');
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}


	public function faltas_nueva($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		$this->load->model('Modelo_admin_notas');
		$data['estudiante'] = $this->Modelo_admin_notas->Get_Nombre_Estudiante($id);
		if ($this->input->post()) {
			$this->Modelo_admin->Nueva_Falta($id, $this->input->post('razon'));
			redirect(base_url('admin/faltas/'));
		} else {
			$this->load->view('admin/admin_faltas_nueva', $data);
		}	

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}


	public function faltas_editar($id) {
		if ($this->logeado_admin()) {

		$this->load->model('Modelo_admin');
		$this->load->model('Modelo_admin_notas');
		$data['modelo'] = $this->Modelo_admin_notas;
		if ($this->input->post()) {
			$this->Modelo_admin->Actualizar_Falta($id, $this->input->post('razon'), $this->input->post('justificada'));
			redirect(base_url('admin/faltas/editar/'.$id.'/'));
		} else {
			$data['faltas'] = $this->Modelo_admin->Get_Falta($id);
			$this->load->view('admin/admin_faltas_editar', $data);
		}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function faltas_ver_todas($id) {
		if ($this->logeado_admin()) {
			$this->load->model('Modelo_admin');
			$this->load->model('Modelo_admin_notas');
			$data['estudiante'] = $this->Modelo_admin_notas->Get_Nombre_Estudiante($id);
			$data['anadir_otra_id'] = $id;
			$data['faltas'] = $this->Modelo_admin->Get_Faltas_Por_Estudiante($id);
			$this->load->view('admin/admin_faltas_ver_todas', $data);
		} else {
			redirect(base_url('admin/ingreso/'));
		}
		
	}

	public function ingreso() {

	if ($this->session->userdata('logeado_admin') == true) {
			redirect(base_url('admin/'));
		}

		if ($this->input->post()) {
			
			$this->form_validation->set_rules('correo', 'Correo', 'trim|valid_email|required|min_length[5]|max_length[150]');
			$this->form_validation->set_rules('contrasena', 'Contraseña', 'required|min_length[5]');

			if ($this->form_validation->run()) {
				$correo = $this->input->post('correo');
				$contrasena = $this->input->post('contrasena');

				$this->load->model('Modelo_admin');
				$ingreso = $this->Modelo_admin->Ingreso($correo, $contrasena);

				if ($ingreso) {
					foreach ($ingreso as $user_key) {
						$id_u = $user_key->id;
					}
					$ses = array(
        				'id'  				=> $id_u,
        				'correo'    		=> $correo,
       					'logeado_admin' 	=> TRUE
					);
					$this->session->set_userdata($ses);
					redirect(base_url('admin/'));
				} else {
					$this->load->view('admin/admin_ingreso');
				}


			} else {
				$this->load->view('admin/admin_ingreso');
			}

		} else {
			$this->load->view('admin/admin_ingreso');
		}		
	}


	public function salir() {
		$this->session->sess_destroy();
		redirect(base_url('admin/ingreso'));		
	}


	public function notas_ver_todas($id) {
		if ($this->logeado_admin()) {
			$this->load->model('Modelo_admin_notas');
			$this->load->model('Modelo_admin_notas');
			$data['estudiante'] = $this->Modelo_admin_notas->Get_Nombre_Estudiante($id);
			$data['anadir_otra_id'] = $id;
			$data['notas'] = $this->Modelo_admin_notas->Get_Notas_Por_Grupo($id);
			$this->load->view('admin/admin_notas_ver_todas', $data);
		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function cuenta() {
		if ($this->logeado_admin()) {
			$this->load->model('Modelo_admin');
			$data['cuentas'] = $this->Modelo_admin->Get_Cuenta_Admin($this->session->userdata('id'));

			if ($this->input->post()) {
				$this->Modelo_admin->Actualizar_Cuenta_Admin($this->session->userdata('id'), $this->input->post('nombre'), $this->input->post('apellidos'));
				redirect(base_url('admin/cuenta/'));
			} else {
				$this->load->view('admin/admin_cuenta', $data);
			}

		} else {
			redirect(base_url('admin/ingreso/'));
		}
	}

	public function cuenta_seguridad() {
		if ($this->logeado_admin()) {
			$this->load->model('Modelo_admin');

			if ($this->input->post()) {

				$this->form_validation->set_rules('contrasena_vieja', 'Contraseña', 'required');
				$this->form_validation->set_rules('contrasena_nueva', 'Contraseña Nueva', 'required|min_length[5]');
				$this->form_validation->set_rules('contrasena_nueva_r', 'Confirmar Contraseña', 'required|matches[contrasena_nueva]');

				if ($this->form_validation->run()) {
			
					$pass = $this->Modelo_admin->Actualizar_Seguridad_Admin($this->session->userdata('id'), $this->input->post('contrasena_vieja'), $this->input->post('contrasena_nueva'));

					if ($pass) {redirect('admin/salir');} else {echo'No Corresponde';}

				} else {
					$this->load->view('admin/admin_cuenta_seguridad');
				}
				
			} else {
				$this->load->view('admin/admin_cuenta_seguridad');
			}

		} else {
			redirect(base_url('admin/ingreso/'));
		}		
	}

	public function definitiva($id) {
		if ($this->logeado_admin()) {
			$this->load->model('Modelo_admin_notas');

			if ($this->input->post()) {
				$this->Modelo_admin_notas->Actualizar_Definitiva($id, $this->input->post('nota'));
				redirect(base_url('admin/definitiva/'.$id.'/'));
			} else {
				$data['estudiante'] = $this->Modelo_admin_notas->Get_Nombre_Estudiante($id);
				$data['definitivas'] = $this->Modelo_admin_notas->Get_Definitiva($id);
				$this->load->view('admin/admin_definitiva', $data);
			}
			
		} else {
			redirect(base_url('admin/ingreso/'));
		}
		
	}


}