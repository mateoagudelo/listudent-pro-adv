<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('menu')) {
	function menu() {
		$contenido = '<div style="margin-bottom: 20px;"><nav class="navbar navbar-default navbar-fixed-top z-depth-1" role="navigation">
<div class="container-fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
<span class="sr-only">Desplegar navegación</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="'.base_url().'estudiantes/">Listudent 2.0!</a>
</div>
<div class="collapse navbar-collapse navbar-ex1-collapse">
<ul class="nav navbar-nav">
<li class="dropdown">
<li><a href="'.base_url().'estudiantes/"> Página principal</b></a></li>
<li><a href="'.base_url().'estudiantes/notas/"> Notas</b></a></li>
<li><a href="'.base_url().'estudiantes/perfiles/"> Mi grupo</b></a></li>
</li>

</ul>
<ul class="nav navbar-nav navbar-right">
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> Cuenta <b class="caret"></b>
</a>

<ul class="dropdown-menu">
<li><a href="'.base_url().'estudiantes/cuenta/">Mi cuenta</a></li>
<li><a href="'.base_url().'estudiantes/bandeja/">Mensajes</a></li>
<li class="divider"></li>
<li><a href="'.base_url().'estudiantes/salir/">Salir</a></li>
</ul>
</li>
</ul>
</div>
</nav></div>';

	echo $contenido;
	}
}


if (!function_exists('link_tag_js')) {
	function link_tag_js($name) {
		echo '<script type="text/javascript" src="'.base_url().$name.'" ></script>';
	}
}

if (!function_exists('link_tag_script')) {
	function link_tag_script($name) {
		echo '<script src="'.base_url().$name.'"></script>';
	}
}









