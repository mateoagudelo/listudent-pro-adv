<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('inicial')) {
	function inicial() {
		$contenido = '	<!-- Basic Page Info -->
		<meta charset="utf-8">
		<title>Listudent > Escritorio</title>

		<!-- Site favicon -->
		<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
		<!-- CSS -->
		<link rel="stylesheet" href="'.base_url().'assets/css/style.css">';

		echo $contenido;
	}
}

if(!function_exists('menu')) {
	function menu() {
		$contenido = '<div class="left-side-bar">
		<div class="brand-logo">
			<h1 style="color: white;">Listudent</h1>
			<a href="index.php">
				<img src="vendors/images/deskapp-logo.png" alt="">
			</a>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li class="dropdown">
						<a href="'.base_url('admin/').'" class="dropdown-toggle no-arrow">
							<span class="fa fa-home"></span><span class="mtext">Inicio</span>
						</a>
					</li>

					<li class="dropdown">
						<a href="'.base_url('admin/grupos/').'" class="dropdown-toggle no-arrow">
							<span class="fa fa-table"></span><span class="mtext">Grupos</span>
						</a>
					</li>

					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="fa fa-user"></span><span class="mtext">Estudiantes</span>
						</a>
						<ul class="submenu">
							<li><a href="'.base_url('admin/estudiantes/todos/').'">Todos</a></li>
							<li><a href="'.base_url('admin/estudiantes/nuevo/').'">Añadir nuevo</a></li>
						</ul>
					</li>

				</ul>
			</div>
		</div>
	</div>';

		echo $contenido;
	}
}

if (!function_exists('barra_sup')) {
	function barra_sup() {
		$contenido = '	<div class="pre-loader"></div>
	<div class="header clearfix">
		<div class="header-right">
			<div class="brand-logo">
				<a href="index.php">
					<img src="vendors/images/logo.png" alt="" class="mobile-logo">
				</a>
			</div>
			<div class="menu-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon"><i class="fa fa-user-o"></i></span>
						<span class="user-name">'.$_SESSION['correo'].'</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="'.base_url().'admin/cuenta/"><i class="fa fa-user-md" aria-hidden="true"></i> Mi cuenta</a>
						<a class="dropdown-item" href="'.base_url().'admin/salir/"><i class="fa fa-sign-out" aria-hidden="true"></i> Salir</a>
					</div>
				</div>
			</div>

			</div>
		</div>
	</div>';
		echo $contenido;
	}
}


if (!function_exists('link_js_admin')) {
	function link_js_admin() {
		echo '<script src="'.base_url().'assets/js/script.js"></script>';
	}
}

if (!function_exists('datatables')) {
	function datatables() {
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/media/css/jquery.dataTables.css">';
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/media/css/dataTables.bootstrap4.css">';
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/media/css/responsive.dataTables.css">';
	}
}

if (!function_exists('datatables_js')) {
	function datatables_js() {
		echo '
	<script src="'.base_url().'assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="'.base_url().'assets/plugins/datatables/media/js/dataTables.bootstrap4.js"></script>
	<script src="'.base_url().'assets/plugins/datatables/media/js/dataTables.responsive.js"></script>
	<script src="'.base_url().'assets/plugins/datatables/media/js/responsive.bootstrap4.js"></script>
	<!-- buttons for Export datatable -->
	<script src="src/plugins/datatables/media/js/button/dataTables.buttons.js"></script>
	<script src="src/plugins/datatables/media/js/button/buttons.bootstrap4.js"></script>
	<script src="src/plugins/datatables/media/js/button/buttons.print.js"></script>
	<script src="src/plugins/datatables/media/js/button/buttons.html5.js"></script>
	<script src="src/plugins/datatables/media/js/button/buttons.flash.js"></script>
	<script src="src/plugins/datatables/media/js/button/pdfmake.min.js"></script>
	<script src="src/plugins/datatables/media/js/button/vfs_fonts.js"></script>';
	}
}


//DEPRECATED 

if (!function_exists('link_tag_js')) {
	function link_tag_js($name) {
		echo '<script type="text/javascript" src="'.base_url().$name.'" ></script>';
		echo '<script src="'.base_url().'assets/js/script.js"></script>';
	}
}

if (!function_exists('link_tag_script')) {
	function link_tag_script($name) {
		echo '<script src="'.base_url().$name.'"></script>';
	}
}


/**  EDITAR GRUPO **/
if (!function_exists('grupos_editar_nombre')) {
	function grupos_editar_nombre($string) {
		return '<input type="text" id="nombre" name="nombre" class="form-control" value="'.$string.'" required="">';
	}
}

if (!function_exists('grupos_editar_descripcion')) {
	function grupos_editar_descripcion($string) {
		return '<textarea rows="3" id="descripcion" name="descripcion" class="form-control" required="">'.$string.'</textarea>';
	}
}

if (!function_exists('grupos_editar_actualizar')) {
	function grupos_editar_actualizar() {
		return '<input type="submit" class="btn btn-success" value="Actualizar">';
	}
}
/** EDITAR GRUPO FIN **/



