<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_admin extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	protected $tabla_usuarios = 'ltd_usuarios';
	protected $tabla_mensajes = 'mensajes';
	protected $tabla_notas = 'ltd_notas';
	protected $tabla_entradas = 'ltd_entradas';
	protected $tabla_faltas = 'ltd_faltas';
	protected $tabla_grupos = 'ltd_grupos';


	public function Get_Table_Usuarios() {
		return $this->tabla_usuarios;
	}

	public function Get_Table_Mensajes() {
		return $this->tabla_mensajes;
	}

	public function Get_Table_Notas() {
		return $this->tabla_notas;
	}

	public function Get_Table_Entradas() {
		return $this->tabla_entradas;
	}

	public function Get_Table_Faltas() {
		return $this->tabla_faltas;
	}

	public function Get_Table_Grupos() {
		return $this->tabla_grupos;
	}


	/*****************************************/

	protected $id, $nombre, $apellidos, $correo, $grupo, $estado_cuenta, $descripcion, $datos, $contrasena, $contrasena_r, $documento, $razon, $justificada, $contenido;

	public function get_Numero_Grupos() {
		$query = $this->db->query('SELECT * FROM '.$this->tabla_grupos);
		return $query->num_rows();
	}

	public function Filas_Grupos() {
		$query = $this->db->get($this->Get_Table_Grupos());
		return  $query->num_rows(); 		
	}

	public function Total_Paginados_Grupos($por_pagina, $segmento) {
		$query = $this->db->get($this->Get_Table_Grupos(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	/** FILTRO DE ESTUDIANTES POR GRUPOS **/

	public function get_Numero_Estudiantes() {
		$query = $this->db->query('SELECT * FROM '.$this->tabla_usuarios.' WHERE rango = 2');
		return $query->num_rows();
	}

	public function Get_Estudiantes_Por_Grupo($id) {
		$this->id = $id;
		$this->db->where('rango', 2);
		$this->db->where('grupo', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios());
		return $query->result();
	}

	/** FIN **/

	public function Editar_Grupo($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Grupos());
		return $query->result();
	}

	public function Actualizar_Grupo($id, $nombre, $descripcion) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->descripcion = $descripcion;
		$this->datos = array('nombre' => $this->nombre);
		$this->db->where('id', $this->id);
		$this->db->update($this->Get_Table_Grupos(), $this->datos);
		return true;
	}

	public function Eliminar_Grupo($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$this->db->delete($this->Get_Table_Grupos());
		return true;
	}

	public function Nuevo_Grupo($nombre) {
		$this->nombre = $nombre;

		$this->datos = array('nombre' => $this->nombre);
		$this->db->insert($this->Get_Table_Grupos(), $this->datos);
		return true;
	}



	/** ESTUDIANTES **/

	public function Filas_Estudiantes() {
		$this->db->where('rango', 2);
		$query = $this->db->get($this->Get_Table_Usuarios());
		return  $query->num_rows(); 		
	}

	public function Total_Paginados_Estudiantes($por_pagina, $segmento) {
		$this->db->where('rango', 2);
		$query = $this->db->get($this->Get_Table_Usuarios(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	public function Editar_Estudiantes($id) {
		$this->id = $id;
		$this->db->select('id, nombre, apellidos, correo, documento, grupo, estado_cuenta');
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios());
		return $query->result();
	}

	
	public function Obtener_Grupo($id) {
		$this->id = $id;
		$this->db->select('nombre');
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Grupos());

		if ($query->result()) {
			foreach ($query->result() as $data) {
				$nombre = $data->nombre;
				return $nombre;
			}
		} else {
			return 'No asignado';
		}

	}

	protected $id_g_p;
	public function Obtener_Grupos($id) {
		$this->id = $id;
		$this->db->select('grupo');
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios());
		
		foreach ($query->result() as $grupo_p) {
			$this->id_g_p = $grupo_p->grupo;
		}

		$this->db->select('id, nombre');
		$this->db->where_not_in('id', $this->id_g_p);
		$query2 = $this->db->get($this->Get_Table_Grupos());
		return $query2->result();
	}

	public function Obtener_Grupos_Nuevo() {
		$this->db->select('id, nombre');
		$query = $this->db->get($this->Get_Table_Grupos());
		return $query->result();
	}

	public function Actualizar_Estudiante($id, $nombre, $apellidos, $correo, $grupo, $documento, $estado_cuenta) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;
		$this->correo = $correo;
		$this->grupo = $grupo;
		$this->documento = $documento;
		$this->estado_cuenta = $estado_cuenta;
		$data = array('nombre' => $this->nombre, 'apellidos' => $this->apellidos, 'correo' => $this->correo, 'grupo' => $this->grupo, 'documento' => $this->documento, 'estado_cuenta' => $this->estado_cuenta);

		$this->db->where('id', $this->id);
		$this->db->update($this->Get_Table_Usuarios(), $data);
		return true;
	}

	public function Nuevo_Estudiante($nombre, $apellidos, $correo, $grupo, $documento) {
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;
		$this->correo = $correo;
		$this->grupo = $grupo;
		$this->documento = $documento;
		$data = array('nombre' => $this->nombre, 'apellidos' => $this->apellidos, 'correo' => $this->correo, 'grupo' => $this->grupo, 'documento' => $this->documento);
		$this->db->insert($this->Get_Table_Usuarios(), $data);

		return true;
	}

	public function Eliminar_Estudiante($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$this->db->delete($this->Get_Table_Usuarios());

		$this->db->where('id_es', $this->id);
		$this->db->delete($this->Get_Table_Faltas());

		$this->db->where('id_es', $this->id);
		$this->db->delete($this->Get_Table_Notas());
		return true;
	}



	/*** FALTAS ***/
	public function Filas_Faltas() {
		$query = $this->db->get($this->Get_Table_Faltas());
		return  $query->num_rows(); 		
	}

	public function Total_Paginados_Faltas($por_pagina, $segmento) {
		$query = $this->db->get($this->Get_Table_Faltas(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	public function Eliminar_Falta($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$this->db->delete($this->Get_Table_Faltas());
		return true;
	}

	public function Nueva_Falta($id_es, $razon) {
		$this->id = $id_es;
		$this->razon = $razon;

		$data = array('id_es' => $this->id, 'contenedor' => 1, 'razon' => $this->razon, 'justificada' => 0);
		$this->db->insert($this->Get_Table_Faltas(), $data);
		return true;
	}

	public function Get_Falta($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Faltas());
		return $query->result();
	}

	public function Actualizar_Falta($id, $razon, $justificada) {
		$this->id = $id;
		$this->razon = $razon;
		$this->justificada = $justificada;
		$data = array('razon' => $this->razon, 'justificada' => $this->justificada);

		$this->db->where('id', $this->id);
		$this->db->update($this->Get_Table_Faltas(), $data);
		return true;
	}


	public function Ingreso($correo, $contrasena) {
		$this->correo = $correo;
		$this->contrasena = sha1($contrasena);

		$this->db->where('correo', $this->correo);
		$this->db->where('contrasena', $this->contrasena);
		$this->db->where('rango', 1);
		$query = $this->db->get($this->Get_Table_Usuarios());

		if ($query->num_rows()==1) {
			return $query->result();
			return true;
		} else { return false; }
	}
	/*** FIN ***/


	/*** ENTRADAS ***/

	public function Filas_Entradas() {
		$query = $this->db->get($this->Get_Table_Entradas());
		return  $query->num_rows(); 		
	}

	public function Total_Paginados_Entradas($por_pagina, $segmento) {
		$query = $this->db->get($this->Get_Table_Entradas(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	public function Eliminar_Entrada($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$this->db->delete($this->Get_Table_Entradas());
		return true;
	}

	public function Nueva_Entrada($nombre, $contenido) {
		$this->nombre = $nombre;
		$this->contenido = $contenido;
		$data = array('nombre' => $this->nombre, 'contenido' => $this->contenido);
		$this->db->insert($this->Get_Table_Entradas(), $data);
		return true;
	}

	public function Get_Entrada($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Entradas());
		return $query->result();
	}

	public function Editar_Entrada($id, $nombre, $contenido) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->contenido = $contenido;

		$data = array('nombre' => $this->nombre, 'contenido' => $this->contenido);
		$this->db->where('id', $this->id);
		$this->db->update($this->Get_Table_Entradas(), $data);
		return true;
	}
	/*** FIN ***/

	public function Get_Faltas_Por_Estudiante($id) {
		$this->id = $id;
		$this->db->where('id_es', $this->id);
		$query = $this->db->get($this->Get_Table_Faltas());
		return $query->result();
	}

	public function Get_Cuenta_Admin($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$this->db->where('rango', 1);
		$query = $this->db->get($this->Get_Table_Usuarios());
		return $query->result();
	}

	public function Actualizar_Cuenta_Admin($id, $nombre, $apellidos) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;

		$data = array('nombre' => $this->nombre, 'apellidos' => $this->apellidos);
		$this->db->where('id', $this->id);
		$this->db->where('rango', 1);
		$this->db->update($this->Get_Table_Usuarios(), $data);
		return true;
	}

	protected $contrasena_vieja, $contrasena_nueva;
	public function Actualizar_Seguridad_Admin($id, $contrasena_vieja, $contrasena_nueva) {
		$this->id = $id;
		$this->contrasena_vieja = sha1($contrasena_vieja);
		$this->contrasena_nueva = sha1($contrasena_nueva);

		$this->db->select('contrasena');
		$this->db->where('id', $this->id);
		$this->db->where('rango', 1);
		$query = $this->db->get($this->Get_Table_Usuarios());

		foreach ($query->result() as $pass) {
			if ($pass->contrasena == $this->contrasena_vieja) {
				$data = array(
        			'contrasena' => $this->contrasena_nueva
				);

				$this->db->where('id', $this->id);
				$this->db->where('rango', 1);
				$this->db->update($this->Get_Table_Usuarios(), $data);
				return true;
			} else {
				return false;
			}
		}

	}

}