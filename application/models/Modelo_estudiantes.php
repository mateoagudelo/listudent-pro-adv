<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_estudiantes extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	//tablas
	protected $tabla_usuarios = 'ltd_usuarios';
	protected $tabla_mensajes = 'mensajes';
	protected $tabla_notas = 'ltd_notas';
	protected $tabla_entradas = 'ltd_entradas';
	protected $tabla_faltas = 'ltd_faltas';

	public function Get_Table_Usuarios() {
		return $this->tabla_usuarios;
	}

	public function Get_Table_Mensajes() {
		return $this->tabla_mensajes;
	}

	public function Get_Table_Notas() {
		return $this->tabla_notas;
	}

	public function Get_Table_Entradas() {
		return $this->tabla_entradas;
	}

	public function Get_Table_Faltas() {
		return $this->tabla_faltas;
	}


	//variables
	protected $id, $nombre, $apellidos, $correo, $contrasena, $contrasena_nueva, $contrasena_vieja, $id_usuario, $des, $fecha, $data, $grupo;

	public function Get_Cuenta($id) {
		$this->id = $id;
		$this->db->select('nombre, apellidos, correo, grupo');
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios());
		return $query->result();
	}

	public function Put_Cuenta($id, $nombre, $apellidos) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;
		$data = array(
        	'nombre' => $this->nombre,
        	'apellidos' => $this->apellidos
		);

		$this->db->where('id', $this->id);
		$this->db->update($this->Get_Table_Usuarios(), $data);
	}

	public function Put_Seguridad($id, $contrasena_vieja, $contrasena_nueva) {
		$this->id = $id;
		$this->contrasena_vieja = sha1($contrasena_vieja);
		$this->contrasena_nueva = sha1($contrasena_nueva);

		$this->db->select('contrasena');
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios());

		foreach ($query->result() as $pass) {
			if ($pass->contrasena == $this->contrasena_vieja) {
				$data = array(
        			'contrasena' => $this->contrasena_nueva
				);

				$this->db->where('id', $this->id);
				$this->db->update($this->Get_Table_Usuarios(), $data);
				return true;
			} else {
				return false;
			}
		}

	}

	public function Nuevo($nombre, $apellidos, $correo, $contrasena) {
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;
		$this->correo = $correo;
		$this->contrasena = sha1($contrasena);

		$data = array(
			'nombre' => $this->nombre,
			'apellidos' => $this->apellidos,
			'correo' => $this->correo,
			'contrasena' => $this->contrasena 
			);

		$this->db->insert($this->Get_Table_Usuarios(), $data);
	}

	public function Ingreso($correo, $contrasena) {
		$this->correo = $correo;
		$this->contrasena = sha1($contrasena);

		$this->db->where('correo', $this->correo);
		$this->db->where('contrasena', $this->contrasena);
		$query = $this->db->get($this->Get_Table_Usuarios());

		if ($query->num_rows()==1) {
			return $query->result();
			return true;
		} else { return false; }
	}

	public function Filas_bandeja($id) {
		$this->id = $id;
		$this->db->where('id_destinatario', $this->id);
		$query = $this->db->get($this->Get_Table_Mensajes());
		return  $query->num_rows(); 
	}

	public function Total_paginados_bandeja($id, $por_pagina, $segmento) {
		$this->id = $id;
		$this->db->where('id_destinatario', $this->id);
		$query = $this->db->get($this->Get_Table_Mensajes(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	protected $usuario_creador;
	public function Get_Mensaje($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Mensajes());
		//return $query->result();

		foreach ($query->result() as $msj) {
			$this->asunto = $msj->asunto;
			$this->mensaje = $msj->mensaje;
			$this->id_usuario = $msj->id_creador;
			$this->fecha = $msj->fecha;
		}

		$this->db->select('nombre, apellidos');
		$this->db->where('id', $this->id_usuario);
		$query2 = $this->db->get($this->Get_Table_Usuarios());

		foreach ($query2->result() as $cre) {
			$this->usuario_creador = $cre->nombre.' '.$cre->apellidos;
		}

		return $this->data = array('id' => $this->id, 'creador' => $this->usuario_creador, 'asunto' => $this->asunto, 'mensaje' => $this->mensaje, 'fecha' => $this->fecha);

	}

	public function Del_Mensaje($id, $id_usuario) {
		$this->id = $id;
		$this->id_usuario = $id_usuario;
		$this->db->select('id_destinatario');
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Mensajes());

		foreach ($query->result() as $mensaje) {
			$this->des = $mensaje->id_destinatario;
		}

		if ($this->des == $this->id_usuario) {
			$this->db->delete($this->Get_Table_Mensajes(), array('id' => $this->id));
			return true;
		} else { return false; }
	}

	protected $asunto, $mensaje;
	public function Nuevo_Mensaje($id_creador, $id_destinatario, $asunto, $mensaje) {
		$this->id_usuario = $id_creador;
		$this->id = $id_destinatario;
		$this->asunto = $asunto;
		$this->mensaje = $mensaje;

		$data = array(
			'id_destinatario' => $this->id,
			'id_creador' => $this->id_usuario,
			'asunto' => $this->asunto,
			'mensaje' => $this->mensaje,
			'fecha' => date("j/n/Y")
			);

		$this->db->insert($this->Get_Table_Mensajes(), $data);
		return true;
	}

	public function Get_Grupo($id) {
		$this->id = $id;
		$this->db->select('grupo');
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios());

		foreach ($query->result() as $grupo) {
			$this->grupo = $grupo->grupo;
		}

		return $this->grupo;
	}

	public function Para_Mensaje($id) {
		$this->id = $id;
		$this->grupo = $this->Get_Grupo($this->id);

		$this->db->select('id, nombre, apellidos');
		$this->db->where('grupo', $this->grupo);
		$this->db->where('rango', 2);
		$this->db->where_not_in('id', $this->id);
		$query2 = $this->db->get($this->Get_Table_Usuarios());
		return $query2->result();
	}

	public function Filas_perfiles($id) {
		$this->id = $id;
		$this->grupo = $this->Get_Grupo($this->id);
		$this->db->where('grupo', $this->grupo);
		$this->db->where('rango', 2);
		$this->db->where_not_in('id', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios());
		return  $query->num_rows(); 		
	}

	public function Total_paginados_perfiles($id, $por_pagina, $segmento) {
		$this->id = $id;
		$this->grupo = $this->Get_Grupo($this->id);
		$this->db->select('id, nombre, apellidos');
		$this->db->where('rango', 2);
		$this->db->where('grupo', $this->grupo);
		$this->db->where_not_in('id', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	public function Filas_notas($id) {
		$this->id = $id;
		$this->db->where('id_es', $this->id);
		$query = $this->db->get($this->Get_Table_Notas());
		return  $query->num_rows(); 
	}

	public function Total_paginados_notas($id, $por_pagina, $segmento) {
		$this->id = $id;
		$this->db->where('id_es', $this->id);
		$query = $this->db->get($this->Get_Table_Notas(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	public function Get_notas_lmt($id) {
		$this->id = $id;
		$this->db->where('id_es', $this->id);
		$this->db->limit(5);
		$query = $this->db->get($this->Get_Table_Notas());
		return $query->result();
	}

	public function Filas_entradas() {
		$query = $this->db->get($this->Get_Table_Entradas());
		return  $query->num_rows(); 
	}

	public function Total_paginados_entradas($por_pagina, $segmento) {
		$query = $this->db->get($this->Get_Table_Entradas(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	public function Get_entradas_lmt() {
		$this->db->limit(5);
		$query = $this->db->get($this->Get_Table_Entradas());
		return $query->result();
	}

	public function Get_entrada($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Entradas());
		return $query->result();
	}

	
	public function Get_faltas_lmt($id) {
		$this->id = $id;
		$this->db->where('id_es', $this->id);
		$this->db->limit(5);
		$query = $this->db->get($this->Get_Table_Faltas());
		return $query->result();
	}


}