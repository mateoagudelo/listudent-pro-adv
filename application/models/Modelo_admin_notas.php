<?php

class Modelo_admin_notas extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	protected $tabla = 'ltd_notas';
	protected $tabla_usuarios = 'ltd_usuarios';
	protected $tabla_definitiva = 'ltd_definitiva';

	public function Get_Table_Notas() {
		return $this->tabla;
	}

	public function Get_Table_Usuarios() {
		return $this->tabla_usuarios;
	}

	public function Get_Table_Definitiva() {
		return $this->tabla_definitiva;
	}


	protected $id, $data, $nombre, $nota;

	public function Filas_Notas() {
		$query = $this->db->get($this->Get_Table_Notas());
		return $query->num_rows(); 		
	}

	public function Total_Paginados_Notas($por_pagina, $segmento) {
		$query = $this->db->get($this->Get_Table_Notas(), $por_pagina, $segmento);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
		    	$data[] = $fila;
			}

            return $data;
        }
	}

	public function Get_Nombre_Estudiante($id) {
		$this->id = $id;
		$this->db->select('nombre, apellidos');
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Usuarios());
		
		foreach ($query->result() as $query1) {
			$this->data = $query1->nombre . ' ' . $query1->apellidos;
		}

		return $this->data;
	}

	public function Eliminar_Nota($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$this->db->delete($this->Get_Table_Notas());
		return true;
	}

	public function Actualizar_Nota($id, $nombre, $nota) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->nota = $nota;

		$this->data = array('nombre' => $this->nombre, 'nota' => $this->nota);

		$this->db->where('id', $this->id);
		$this->db->update($this->Get_Table_Notas(), $this->data);
		return true;
	}

	public function Get_Nota($id) {
		$this->id = $id;
		$this->db->where('id', $this->id);
		$query = $this->db->get($this->Get_Table_Notas());
		return $query->result();
	}

	public function Nueva_Nota($id, $nombre, $nota) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->nota = $nota;

		$this->data = array('id_es' => $this->id, 'nombre' => $this->nombre, 'nota' => $this->nota);
		$this->db->insert($this->Get_Table_Notas(), $this->data);
		return true;
	}

	public function Get_Notas_Por_Grupo($id) {
		$this->id = $id;
		$this->db->where('id_es', $this->id);
		$query = $this->db->get($this->Get_Table_Notas());
		return $query->result();
	}

	public function Get_Definitiva($id) {
		$this->id = $id;
		$this->db->where('id_es', $this->id);
		$query = $this->db->get($this->Get_Table_Definitiva());
		
		if ($query->result()) {
			return $query->result();
		} else {
			$data = array('id_es' => $this->id, 'nota' => 0);
			$this->db->insert($this->Get_Table_Definitiva(), $data);

			$this->db->where('id_es', $this->id);
			$query = $this->db->get($this->Get_Table_Definitiva());
			return $query->result();
		}

	}

	public function Actualizar_Definitiva($id, $nota) {
		$this->id = $id;
		$this->nota = $nota;
		$this->db->where('id_es', $this->id);

		$data = array('nota' => $this->nota);

		$this->db->update($this->Get_Table_Definitiva(), $data);
		return true;
	}

}