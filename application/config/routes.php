<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/**** ADMINISTRADOR ****/
$route['admin/grupos/(:num)'] = 'admin/grupos';
$route['admin/grupos/editar/(:num)'] = 'admin/grupos_editar/$1';
$route['admin/grupos/eliminar/(:num)'] = 'admin/grupos_eliminar/$1';
$route['admin/grupos/nuevo'] = 'admin/grupos_nuevo';
$route['admin/grupos/ver/(:num)'] = 'admin/grupos_ver/$1';


$route['admin/estudiantes/(:num)'] = 'admin/estudiantes';
$route['admin/estudiantes/editar/(:num)'] = 'admin/estudiantes_editar/$1';
$route['admin/estudiantes/eliminar/(:num)'] = 'admin/estudiantes_eliminar/$1';
$route['admin/estudiantes/nuevo'] = 'admin/estudiantes_nuevo';


$route['admin/notas/eliminar/(:num)'] = 'admin/notas_eliminar/$1';
$route['admin/notas/editar/(:num)'] = 'admin/notas_editar/$1';
$route['admin/notas/nueva/(:num)'] = 'admin/notas_nueva/$1';
$route['admin/notas/ver_todas/(:num)'] = 'admin/notas_ver_todas/$1';


$route['admin/faltas/eliminar/(:num)'] = 'admin/faltas_eliminar/$1';
$route['admin/faltas/nueva/(:num)'] = 'admin/faltas_nueva/$1';
$route['admin/faltas/editar/(:num)'] = 'admin/faltas_editar/$1';
$route['admin/faltas/ver_todas/(:num)'] = 'admin/faltas_ver_todas/$1';


$route['admin/cuenta'] = 'admin/cuenta';
$route['admin/cuenta/seguridad'] = 'admin/cuenta_seguridad';